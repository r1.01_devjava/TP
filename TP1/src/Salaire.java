class Salaire{
	void principal(){
		int salaire;
		double	salaire_deduction;
		double assurance_maladie;
		double assurance_vieillesse_déplafonnée;
		double Contribution_sociale_généralisée;
		double CRDS;
		double assurance_vieillesse_plafonnée;
		double chomage;
		double total_prelevement;
		double	salaire_percu;
		
		salaire = SimpleInput.getInt("Entrez le salaire brut :");
		
		salaire_deduction= salaire -0.0175*salaire;
		Contribution_sociale_généralisée = 0.075*salaire_deduction;
		CRDS=0.05*salaire_deduction;
		assurance_maladie =  0.075*salaire;
		assurance_vieillesse_déplafonnée = 0.001*salaire ;
		assurance_vieillesse_plafonnée = 0.0675*salaire ;
		chomage = 0.024*salaire;
		total_prelevement=Contribution_sociale_généralisée+CRDS+assurance_maladie+assurance_vieillesse_déplafonnée+assurance_vieillesse_plafonnée+chomage;
		salaire_percu=salaire - total_prelevement;
		
		System.out.println( "Le salaire brut est\t " +salaire);
		System.out.println( "Contribution_sociale_généralisée\t " +Contribution_sociale_généralisée);
		System.out.println( "CRDS\t" +CRDS);
		System.out.println( "assurance_maladie\t" +assurance_maladie);
		System.out.println( "assurance_vieillesse_déplafonnée\t" +assurance_vieillesse_déplafonnée);
		System.out.println( "assurance_vieillesse_plafonnée\t" +assurance_vieillesse_plafonnée);
		System.out.println( "chomage\t" +chomage);
		System.out.println( "total_prelevement\t" +total_prelevement);
		System.out.println( "salaire_percu\t" +salaire_percu);
		
	}
}
