class Exo1TP4{
	void principal(){
		testFactoriel ();
		testCombinaison();
	}
	
	
	/** Méthode factorielle
	 * @param n valeur de la factoriel à calculer
	 * @return factoriel de n
	 **/
	int factoriel (int n){
		int fac;
		fac = 1;
		while ( n > 1){
			fac = fac * n;
			n = n - 1;
		}
		return fac;
	}
	
	/*** calcul de la combinaison k parmi n
	* @param n cardinalité de l’ensemble
	* @param k nombre d’éléments dans n avec k<=n
	* @return nombre de combinaisons de k parmi n
	*/	
	int combinaison (int n,int k){
		int ret;
		ret = factoriel(n)/(factoriel(k)* factoriel(n-k));
		return ret;
	}
	
	
	/*** Teste la méthode factoriel()*/
	void testFactoriel () {
		System.out.println ();
		System.out.println ("*** testFactoriel()");
		testCasFactoriel (5, 120);
		testCasFactoriel (0, 1);
		testCasFactoriel (1, 1);
		testCasFactoriel (2, 2);
	}
	
	/** teste un appel de factoriel
	 * @param n valeur de la factoriel à calculer
	 * @param result resultat attendu
	 */
	 void testCasFactoriel (int n, int result) {
		// Arrange
			System.out.print ("factoriel (" + n + ") \t= " + result + "\t : ");
		 // Act
		 int resExec = factoriel(n);
		 // Assert			 
		 if (resExec == result){
			 System.out.println ("OK");
		} else {
			System.err.println ("ERREUR");
		}
	}
	
	
	/*** Teste la méthode combinaison*/
	void testCombinaison() {
		System.out.println ();
		System.out.println ("*** testCombinaison()");
		testCasCombinaison (5,3,10);
		testCasCombinaison (3,0,1);
		testCasCombinaison (6,4,15);
		testCasCombinaison (7,7,1);
		testCasCombinaison (3,1,3);
		testCasCombinaison (0,1,1);
		testCasCombinaison (0,0,1);
	}
	/*** teste un appel de combinaison
	 * @param n cardinalité de l’ensemble
	 * @param k nombre d’éléments dans n avec k<=n
	 * @param result resultat attendu
	 * **/
	 void testCasCombinaison (int n, int k, int result) {
		// Arrange
			System.out.print ("combinaison (" + n +","+ k + ") \t= " + result + "\t : ");
		 // Act
		 int resExec = combinaison(n,k);
		 // Assert			 
		 if (resExec == result){
			 System.out.println ("OK");
		} else {
			System.err.println ("ERREUR");
		}
	}
}
