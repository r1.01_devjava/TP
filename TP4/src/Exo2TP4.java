class Exo2TP4{
	void principal(){
		 testDiviseur ();
	}
	
	/**
	 * teste la divisibilité de deux entiers
	 * @param p entier positif à tester pour la divisibilité
	 * @param q diviseur strictement positif
	 * @return vrai ssi q divise p
	 */
	boolean estDiviseur ( int p, int q){
		boolean divise;
		divise = false;
		if ( p % q == 0){
			divise = true;
		}
		return divise;
	}
	
	/*** Teste la méthode estDviseur()*/
	void testDiviseur () {
		System.out.println ();
		System.out.println ("*** testDiviseur()");
		testCasDiviseur (5, 120, false);
		testCasDiviseur (19, 1, true);
		testCasDiviseur (1, 1, true);
		testCasDiviseur (8, 2, true);
		testCasDiviseur (15, 6, false);
	}
	
	
	/*** teste un appel de estDiviseur
	 * @param p entier positif à tester pour la divisibilité
     * @param q diviseur strictement positif
     * @param result le resultat attendu
	 * **/
	 void testCasDiviseur (int p,int q, boolean result) {
		// Arrange
			System.out.print ("estDiviseur (" + p +","+ q +") \t= " + result + "\t : ");
		 // Act
		 boolean resExec = estDiviseur(p,q);
		 // Assert			 
		 if (resExec == result){
			 System.out.println ("OK");
		} else {
			System.err.println ("ERREUR");
		}
	}
	
	
}	
