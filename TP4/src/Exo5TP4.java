class Exo5TP4{
	void principal (){
		testCroissant (); 	
	}
	/*** teste si les valeurs d’un tableau sont triées par ordre croissant
	 * @param t tableau d’entiers
	 * @return vrai ssi les valeurs du tableau sont en ordre croissant
	*/
	boolean estCroissant ( int[] t ){
		boolean croissant = true;
		int i;
		i = 1;
		while (croissant && i <= t.length){
			if (t[i-1] >= t[i]){
				croissant = false;
			}
			i = i + 1; 
		}
		return croissant;
	}


	/*** Teste la méthode estCroissant()*/
	void testCroissant () {
		System.out.println ();
		System.out.println ("*** testDiviseur()");
		int[] tab = {1,1,1,2,3,4};
		testCasCroissant(tab,true);
		//testCasCroissant ({1,2,3,4}, true);
		//testCasCroissant ({}, true);
		//testCasCroissant ({1,2,3,4,3}, false);
		//testCasCroissant ({2,1,3,4,5}, false);
	}
	
	
	/*** teste un appel de estDiviseur
	 * @param p entier positif à tester pour la divisibilité
     * @param q diviseur strictement positif
     * @param result le resultat attendu
	 * **/
	 void testCasCroissant (int[] t, boolean result) {
		// Arrange
			System.out.print ("estCroissant (" + t +") \t= " + result + "\t : ");
		 // Act
		 boolean resExec = estCroissant(t);
		 // Assert			 
		 if (resExec == result){
			 System.out.println ("OK");
		} else {
			System.err.println ("ERREUR");
		}
	}
}


