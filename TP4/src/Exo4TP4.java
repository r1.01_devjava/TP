class Exo4TP4{
	void principal(){
		QuatreNbParfaits();
	}
	
	/**
	 * teste la divisibilité de deux entiers
	 * @param p entier positif à tester pour la divisibilité
	 * @param q diviseur strictement positif
	 * @return vrai ssi q divise p
	 */
	boolean estDiviseur ( int p, int q){
		boolean divise;
		divise = false;
		if ( p % q == 0){
			divise = true;
		}
		return divise;
	}
	

	/** teste si un nombre est parfait
	 * @param a entier positif
	 * @return vrai ssi a est un nombre parfait
	 */
	 boolean estParfait ( int a){
		 boolean parfait;
		 int i;
		 int resultat;
		 resultat = 0 ;
		 i = 1;
		 parfait = false;
		 while ( i <= a/2){
			 if ( estDiviseur(a,i)){
				 resultat = resultat + i;
			}
			i = i + 1;
		}
		if ( resultat == a){
			parfait = true;
		}
		return parfait;
	}
	
	/** affcihe les 4 premiers nombres parfaits
	*/
	void QuatreNbParfaits(){
		int i;
		int compteur; 
		compteur = 0;
		i = 1;
		while (i != -1 && compteur != 4 ){
			if (estParfait(i)){
				compteur = compteur + 1;
				System.out.println(i);
			}
			i = i + 1;
		}
	}
}
				 
