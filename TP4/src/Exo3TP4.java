class Exo3TP4{
	void principal (){
		testParfait ();
	
	}

	/**
	 * teste la divisibilité de deux entiers
	 * @param p entier positif à tester pour la divisibilité
	 * @param q diviseur strictement positif
	 * @return vrai ssi q divise p
	 */
	boolean estDiviseur ( int p, int q){
		boolean divise;
		divise = false;
		if ( p % q == 0){
			divise = true;
		}
		return divise;
	}
	
	/*** Teste la méthode estDiviseur()*/
	void testDiviseur () {
		System.out.println ();
		System.out.println ("*** testDiviseur()");
		testCasDiviseur (5, 120, false);
		testCasDiviseur (19, 1, true);
		testCasDiviseur (1, 1, true);
		testCasDiviseur (8, 2, true);
		testCasDiviseur (15, 6, false);
	}
	
	
	/*** teste un appel de estDiviseur
	 * @param p entier positif à tester pour la divisibilité
     * @param q diviseur strictement positif
     * @param result le resultat attendu
	 * **/
	 void testCasDiviseur (int p,int q, boolean result) {
		// Arrange
			System.out.print ("estDiviseur (" + p +","+ q +") \t= " + result + "\t : ");
		 // Act
		 boolean resExec = estDiviseur(p,q);
		 // Assert			 
		 if (resExec == result){
			 System.out.println ("OK");
		} else {
			System.err.println ("ERREUR");
		}
	}
	
	
	/** teste si un nombre est parfait
	 * @param a entier positif
	 * @return vrai ssi a est un nombre parfait
	 */
	 boolean estParfait ( int a){
		 boolean parfait;
		 int i;
		 int resultat;
		 resultat = 0 ;
		 i = 1;
		 parfait = false;
		 while ( i <= a/2){
			 if ( estDiviseur(a,i)){
				 resultat = resultat + i;
			}
			i = i + 1;
		}
		if ( resultat == a){
			parfait = true;
		}
		return parfait;
	}
	
	/*** Teste la méthode estParfait()*/
	void testParfait () {
		System.out.println ();
		System.out.println ("*** testParfait()");
		testCasParfait (5, false);
		testCasParfait (1,false);
		testCasParfait (497,false);
		testCasParfait (6,true);
		testCasParfait (28,true);
		testCasParfait (1,false);
	}
	
	
	/*** teste un appel de estParfait
	 * @param a l'entier a tester si parfait
     * @param result le resultat attendu
	 * **/
	 void testCasParfait (int a, boolean result) {
		// Arrange
			System.out.print ("estParfait (" + a +") \t= " + result + "\t : ");
		 // Act
			boolean resExec = estParfait(a);
		 // Assert			 
		 if (resExec == result){
			 System.out.println ("OK");
		} else {
			System.err.println ("ERREUR");
		}
	}
	
}	
