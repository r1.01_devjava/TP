import java.util.Arrays;
class Exo3TP6{
    void principal (){
        testeliminerDouble ();
    
    }
    /** élimine les valeurs en plusieurs exemplaires dans un tableau
     * un élément présent plusieurs fois n’est plus qu’en un seul exemplaire
     * @param tab tableau d’entiers
     * @return le nombre d’éléments du tableau sans double
     */
    /**int eliminerDouble(int[] tab){
         int ouMettreValeur = tab.length - 1;
         for(int i = 0; i < ouMettreValeur; i++){
             for(int j = 1; j < ouMettreValeur; j++){
                 if ( tab [i] == tab [j] ){
                    System.out.println(ouMettreValeur);
                    int tmp = tab[j];
                    tab[j] = tab[ouMettreValeur];
                    tab[ouMettreValeur ] = tmp;
                    ouMettreValeur = ouMettreValeur - 1;
                }else {
                    System.out.println("pas passé dans le if");
                }
            }
        }
        System.out.println(Arrays.toString(tab));
        System.out.println(ouMettreValeur);
        return ouMettreValeur;
    }*/
    
        
    
    /*** élimine les valeurs en plusieurs exemplaires dans un tableau
     * un élément présent plusieurs fois n’est plus qu’en un seul exemplaire
     * @param tab tableau d’entiers
     * @return le nombre d’éléments du tableau sans double
     **/
    int eliminerDouble(int[] tab) {
        int length = tab.length;
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                if (tab[i] == tab[j]) {
                    for (int k = j; k < length - 1; k++) {
                        tab[k] = tab[k + 1];
                    }
                    j--;
                    length--;
                }
            }
        }
        return length;
    }
    
    
             
         
         
         
         
    
    /** teste de eliminerDouble ()*/
    
    void testeliminerDouble (){
        System.out.println();
        System.out.println("*** eliminerDouble () ***");
        int []tab1 = {0,1,2,3,4};
        testCaseliminerDouble (tab1,5);
        int []tab2 = {0,0,2,3,4};
        testCaseliminerDouble (tab2,4);
        int []tab3 = {0,1,2,3,3};
        testCaseliminerDouble (tab3,4);
        int []tab4 = {0,1,3,3,4};
        testCaseliminerDouble (tab4,4);
        int []tab5 = {0,0,0,0,0};
        testCaseliminerDouble (tab5,1);
        int []tab6 ={};
        testCaseliminerDouble (tab6,0);
    
    }
    
    void testCaseliminerDouble ( int [] tab, int result){
        //Arrange
        System.out.println( "eliminerDouble("+ Arrays.toString(tab)+"\t " + result );
        //Act
        int resExpec = eliminerDouble ( tab);
        //Assert
        if ( resExpec == result){
            System.out.println("OK");
        } else{
            System.err.println("ERREUR");
        }
    }

}
    
