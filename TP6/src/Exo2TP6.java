import java.util.Arrays;
class Exo2TP6{
    void principal(){
        testsontTousDiff();

    }
    /*** vérifie si deux tableaux n’ont aucune valeur commune
     * @param tab1 premier tableau
     * @param tab2 deuxième tableau
     * @return vrai si les tableaux tab1 et tab2 n’ont aucune valeur commune, faux sinon
     */
    
    boolean sontTousDiff(int[] tab1,int[] tab2){
        boolean absent = true ;
        int i = 0;
        int j;
        while ( i < tab1.length && absent){
            j = 0;
            while ( j < tab2.length && absent){
                if (tab1[i] == tab2[j]){
                    absent = false;
                }
                j = j + 1;
            }
            i = i + 1;
        }
        return absent;
    }
    /*** Teste la méthode sontTousDiff*/
	void testsontTousDiff() {
		System.out.println ();
		System.out.println ("*** testsontTousDiff");
        int []t1 = {1,2,3,4,5};
        int []t2 = {0,6,12,18,24};
		testCassontTousDiff (t1,t2, true);
        int []t3 = {0,6,12,18,5};
		testCassontTousDiff (t1,t3, false);
        int []t4 = {1,6,12,18,24};
		testCassontTousDiff (t1,t4, false);
        int []t5 = {0,6,3,18,24};
		testCassontTousDiff (t1,t5, false);
        int []t6 = {1,6,3,18,5};
		testCassontTousDiff (t1,t6, false);
        int []t7 = {1,2,3,4,5};
		testCassontTousDiff (t1,t7, false);
        int []t8 = {};
		testCassontTousDiff (t1,t8, true);
	}
	
	
	/*** teste un appel de estParfait
	 * @param a l'entier a tester si parfait
     * @param result le resultat attendu
	 * **/
	 void testCassontTousDiff (int[] tab1,int[] tab2, boolean result) {
		// Arrange
			System.out.print ("sontTousDiff (" + Arrays.toString(tab1) + "," + Arrays.toString(tab2) +") \t= " + result + "\t : ");
		 // Act
			boolean resExec = sontTousDiff(tab1,tab2);
		 // Assert			 
		 if (resExec == result){
			 System.out.println ("OK");
		} else {
			System.err.println ("ERREUR");
		}
	}

}


    
