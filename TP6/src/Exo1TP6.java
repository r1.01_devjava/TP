import java.util.Arrays;
class Exo1TP6 {
    void principal(){
        testsaisirEtTrier ();
    }
    /*** Crée et saisit un tableau trié de LG_TAB entiers
     * @return tableau trié de LG-TAB entiers
     */
    int[] saisirEtTrier () {
        int LG_TAB= 5;
        int[] t = new int[LG_TAB];
        int i = 0;
        while (i < t.length) {
            t[i] = SimpleInput.getInt ("Entrer un entier");
            // insertion de la valeur en ordre croissant dans t
            int j = i;
            boolean place = false;
            while (place == false && j >=1){
                if ( t[j-1] > t[j]){
                    int tmp = t[j-1];
                    t[j-1] = t[j];
                    t[j]=tmp;
                }else{
                    place =true;
                }
            j = j - 1;
            }
        i = i + 1;
        }
        //System.out.println(Arrays.toString(t));
        return t;
    }
    /**teste la méthode saisirEtTier*/
    void testsaisirEtTrier (){
        System.out.println ();
        System.out.println("***saisirEtTrier()***");
        int []t1 = {1,2,3,4,5};
        testCasSaisirEtTrier(t1,"Taper les valeurs : 1,2,3,4,5 dans le mauvais ordre ");
        int []t2 = {1,2,3,4,5};
        testCasSaisirEtTrier(t2,"Taper les valeurs : 5,1,2,3,4 ");
        int []t2bis = {1,2,3,4,5};
        testCasSaisirEtTrier(t2bis,"Taper les valeurs : 1,2,3,5,4");
        int []t3 = {0,0,0,0,0};
        testCasSaisirEtTrier(t3,"Taper les valeurs : 0");
        int []t4 = {1,2,3,3,3};
        testCasSaisirEtTrier(t4,"Taper les valeurs : 1,3,3,3,2");
    }
    /*** teste un appel de saisir
	 * **/
	 void testCasSaisirEtTrier (int [] t2, String msg) {
		// Arrange
			System.out.print ("saisirEtTrier () \t= " + msg + "\t : ");
		 // Act
		 int[] resExec = saisirEtTrier();
         System.out.println(Arrays.toString(resExec));
		 // Assert			 
		 if (Arrays.equals(resExec, t2)){
			 System.out.println ("OK");
		} else {
			System.err.println ("ERREUR");
		}
	}
}
