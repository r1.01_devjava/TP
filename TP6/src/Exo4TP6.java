import java.util.Arrays;
class Exo4TP6{
    void principal(){
        testestSousChaine();
         
    }
    
    /*** teste si une chaîne est une sous-chaîne d’une autre
     * @param mot chaîne de caractères
     * @param phrase chaîne de carectères
     * @return vrai ssi la première chaîne est présente dans la seconde*/
    boolean estSousChaine (String mot, String phrase){
        boolean estDans = false ;
        String[] motDansPhrase = phrase.split(" ");
        for ( int i = 0; i < motDansPhrase.length; i++){
            if ( mot.equals(motDansPhrase[i])){
                estDans = true;
            
            }
            
        }
        return estDans;
    }
        
        
        

    /** teste de estSousChaine*/
    void testestSousChaine(){
        System.out.println();
        System.out.println("*** testestSousChaine () ***");
        String mot1 = "le";
        String phrase1 = " J'aime le java ";
        testCasestSousChaine (mot1,phrase1,true);
        String mot2 = " chocolat";
        testCasestSousChaine (mot2,phrase1,false);
        String mot3 = " lejava";
        testCasestSousChaine (mot3,phrase1,false);
        String mot4 = " ";
        testCasestSousChaine (mot4,phrase1,false);
        String phrase2 = " ";
        testCasestSousChaine (mot1,phrase2,false);
    
    }
    
    void testCasestSousChaine ( String mot, String phrase, boolean result){
        //Arrange
        System.out.println( "estSousChaine("+ mot +","+ phrase + "\t " + result );
        //Act
        boolean resExpec = estSousChaine (mot,phrase);
        //Assert
        if ( resExpec == result){
            System.out.println("OK");
        } else{
            System.err.println("ERREUR");
        }
    }
        
    

       
    
}
