/**
* Auteur: Bellec Alan
* jeu du Pendu
*/
import java.util.Arrays;
import java.lang.Math;
class Pendu {
	  void principal(){
	  //testAll();
	  partie(creerDico());
	  }
	  void testAll (){
		  testcreerDico ();
		  testchoisirMot ();
		  testAfficherReponse ();
		  testcreerReponse();
		  testTester ();
		  testestComplet ();
	  }
	  
	  /**
	   * Création d'un dictionnaire de mots
	   * @return dictionnaire initialisé
	   */
	   String[] creerDico () {
		 String[] dico = {"acajou","coccyx","hormis","seisme","caiman","igloo","thym","rhum"};
		 return dico;
	   }
	   
	   /**
		* Teste creer Dico
		*/
		void testcreerDico (){
			System.out.println ("*** testcreerDico");
			System.out.println(Arrays.toString(creerDico()));
	   }
	   
	   /**
		* choix aléatoire d'un mot du dictionnaire
		* @param dico dictionnaire des mots à choisir
		* @return chaine choisie de manière aléatoire
		*/
		String choisirMot (String[] dico){
			String mot;
			int nbAleatoire;
		    nbAleatoire = (int)(Math.random() * (dico.length));
			mot = dico[nbAleatoire];
			return mot;
		}
		
		/**
		* teste la méthode choisir mot
		*/
		void testchoisirMot () {
			System.out.println ();
			System.out.println ("*** testchoisirMot");
			System.out.println(choisirMot (creerDico()));
            System.out.println(choisirMot (creerDico()));
            System.out.println(choisirMot (creerDico()));
            System.out.println(choisirMot (creerDico()));
            System.out.println(choisirMot (creerDico()));
            System.out.println(choisirMot (creerDico()));
            System.out.println(choisirMot (creerDico()));
        }
		
		/*** affiche la réponse du joueur
		* @param reponse reponse du joueur
		*/
		void afficherReponse(char[] reponse) {
			for (int i = 0 ; i < reponse.length; i++) {
				System.out.print (reponse[i] + " ");
			}
			System.out.println ();
		}
		
		/*** Test de afficheReponse()*/
		 void testAfficherReponse () {
			 System.out.println ();
			 System.out.println ("*** testAfficherReponse()");
			 char[] reponse1 = {'a','c','a','j','o','u'};
			 testCasAfficherReponse (reponse1);
			 char[] reponse2 = {};
			 testCasAfficherReponse (reponse2);
		}


		/*** teste un appel à afficherReponse()
		* @param reponse tableau des réponse à afficher
		*/
		void testCasAfficherReponse (char[] reponse) {
			System.out.print ("afficherReponse (" + Arrays.toString(reponse) + ") : ");
			afficherReponse (reponse);
		}
		
		/*** création d’un tableau de reponse contenant des ’_’
		 * @param lg longueur du tableau à créer
		 * @return tableau de reponse contenant des ’_’
		 */
		 char[] creerReponse(int lg){
			 char[] reponse;
			 reponse = new char[lg];
			 for(int i = 0; i < lg;i++){
				 reponse[i] = '_';
			 }
			 return reponse;
		 }
		 
		 /**Teste méthode creerReponse*/
		 void testcreerReponse (){
			 System.out.println("*** testcreerReponse ***");
			 System.out.println(creerReponse(5));
			 System.out.println(creerReponse(0));
			 System.out.println(creerReponse(1));
			 System.out.println(creerReponse(11));
		}
			
		/*** teste la présence d’un caractère dans le mot* et le place au bon endroit dans réponse
		 * @param mot mot à deviner
		 * @param reponse réponse à compléter si le caractère est présent dans le mot
		 * @param car caractère à chercher dans le mot
		 * @return vrai ssi le caractère est dans le mot à deviner
		 */	 
		boolean tester (String mot, char[] reponse, char car){
			boolean estDansMot;
			estDansMot = false;
			for (int i=0; i < mot.length();i++){
				if (car == mot.charAt(i)){
					reponse[i] = car;
					estDansMot = true;
					
				}
			}
			return estDansMot;
		}
		
		/**teste de la méthode tester
		 */
		void testTester () {
			System.out.println ();
			System.out.println ("*** testTester()");
			testCastester ("gimgembre",creerReponse(9),'i',true);
			testCastester ("gimgembre",creerReponse(9),'z',false);
			testCastester ("gimgembre",creerReponse(9),'e',true);
			testCastester ("",creerReponse(0),'g',false);
			testCastester ("g",creerReponse(1),'g',true);
			//tester avec un tableau new char[]{'_'}
		}
	
	
		/*** teste un appel de tester
		 * @param mot mot à deviner
		 * @param reponse réponse à compléter si le caractère est présent dans le mot
		 * @param car caractère à chercher dans le mot
		 * @param result le resultat attendu
		 */
		void testCastester (String mot, char[] reponse, char car,boolean result) {
			 // Act
				boolean resExec = tester(mot,reponse,car);
			// Arrange
				System.out.print ("tester (" + mot +") \t= reponse :"+ Arrays.toString(reponse)+"car\t"+ car + "\tresult:" + result + "\t : ");
			
			 // Assert			 
			 if (resExec == result){
				 System.out.println ("OK");
			} else {
				System.err.println ("ERREUR");
			}
		}
		
		/*** rend vrai ssi le mot est trouvé
		 * @param mot mot à deviner
		 * @param reponse réponse du joueur
		 * @return vrai ssi le mot est égal caractère par caractère à laréponse
		 */
		boolean estComplet (String mot, char[] reponse){
			boolean motComplet;
			int cbDans;
			cbDans = 0;
			motComplet = false;
			for(int i = 0; i < mot.length();i++){
				if (mot.charAt(i) == reponse[i]){
					cbDans = cbDans + 1;
				}
			}
			if (cbDans == mot.length()){
				motComplet = true;
			}
			return motComplet;
		}
		
		/**teste de la méthode estComplet
		 */
		void testestComplet () {
			System.out.println ();
			System.out.println ("*** testestComplet()");
			testCasestComplet ("gimgembre",new char[]{'g','i','m','g','e','m','b','r','e'},true);
			testCasestComplet ("gimgembre",new char[]{'g','_','m','g','e','m','b','r','e'},false);
			testCasestComplet ("gimgembre",new char[]{'g','i','m','g','e','m','b','r','_'},false);
			testCasestComplet ("gimgembre",new char[]{'_','i','m','g','e','m','b','r','e'},false);
			testCasestComplet ("",new char[]{' '},true);
			testCasestComplet ("g",new char[]{'g'},true);
			//tester avec un tableau new char[]{'_'}
		}
	
	
		/*** teste un appel de estComplet
		 * @param mot mot à deviner
		 * @param tableau réponse completer ou non
		 * @param result le resultat attendu
		 */
		void testCasestComplet (String mot, char[] reponse,boolean result) {
			 // Act
				boolean resExec = estComplet(mot,reponse);
			// Arrange
				System.out.print ("estComplet(" + mot +") \t= reponse :"+ Arrays.toString(reponse)+"\tresult:" + result + "\t : ");
			
			 // Assert			 
			 if (resExec == result){
				 System.out.println ("OK");
			} else {
				System.err.println ("ERREUR");
			}
		}
		
		/*** lancement d’une partie du jeu du pendu
		 * @param dico dictionnaire des mots à deviner
		 */
		void partie(String[] dico){
			String mot = choisirMot(dico);
			int vie = 9;
			char[] reponse = creerReponse(mot.length());
			char carTape;
			//condition sortie: vie =0 || estComplet
			while ( vie !=0 && !estComplet(mot,reponse)){
				afficherReponse(reponse);
				carTape = SimpleInput.getChar("entrez une lettre:");
				tester(mot,reponse,carTape);
							
				if (!tester(mot,reponse,carTape)){
					vie = vie - 1;
					System.out.println("Il vous reste\t"+ vie +" vies");
					}
			}
			if (estComplet(mot,reponse)){
				System.out.println(" Vous avez gagné !\t il vous restait\t"+ vie +"\t vies");
			}else{
				System.out.println(" Vous avez perdu ! ");
			}
		}
		 
}
