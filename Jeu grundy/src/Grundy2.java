/**
* Auteur: Bellec Alan
* jeu de Grundy
*/
import java.util.Arrays;
class Grundy2{
    void principal (){
        //testAll();
        
        System.out.println(" Bienvenue au jeu du Grundy ");
        int type = choixTypeJeu ();
        if (type == 1){
            partieJoueur ();
        } else if ( type ==2 ) {
            partieOrdinateurAleatoire();
        } else {
            partieOrdinateurOptimise();
        }
        
    }
    
    /** Méthode contenant tous les tests*/
    void testAll(){
        testTasDepart ();
        testAffichageTas();
        testchangementJoueur();
        testDiviseTableau ();
        testEstFini();
        testTourOrdinateur ();
        testTourOrdinateurOpti ();
        
    }
    
    /**
    * Création d'un tableau contenant le nombre de batons de la partie
    * @return tableau contenant le nombre de batons voulus
    */
    int[] tasDepart (){
        
        int nbDepart;
        nbDepart = SimpleInput.getInt(" Entrez le nombre de batons pour"+
                " cette partie (au moins 3) :");
        while (nbDepart < 3){
            nbDepart = SimpleInput.getInt(" Entrez le nombre de batons pour"+
                " cette partie (au moins 3) :");
        }
        int[] tas = new int[nbDepart-1];
        tas[0] = nbDepart;
        
        return tas;
    }
    
    /** Methode test tasDepart */
    void testTasDepart (){
        System.out.println(Arrays.toString(tasDepart()));
    }
    
    /**
     * Change de joueur
     * @param joueur ayant joué
     * @param joueur1
     * @param joueur2
     * @return joueur devant jouer
     */
    String changementJoueur (String joueurActuel, String joueur1, String joueur2){
        if ( joueurActuel == joueur1){
            joueurActuel = joueur2;
        }else {
            joueurActuel = joueur1;
        }
        return joueurActuel;
    }
    
    /** Methode de test de Changementjoueur
     */
     void testchangementJoueur(){
         System.out.println ();
         System.out.println ("*** testchangementJoueur()");
         testCasChangementJoueur("alan","alan","louis","louis");
         testCasChangementJoueur("louis","alan","louis","alan");
    }
    
    /*** teste un appel de tester
    * @param joueurActuel
    * @param joueur1
    * @param joueur2
    * @param result le resultat attendu
    */
    void testCasChangementJoueur (String joueurActuel, String joueur1,
                                  String joueur2,String result) {
        
        // Arrange
        System.out.print ("changementJoueur (" + joueurActuel +", "+ joueur1 +
            ", "+ joueur2 +")");
		// Act
        String resExec = changementJoueur ( joueurActuel,joueur1, joueur2);	
        // Assert			 
        if (resExec == result){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
    
    /**
     * Affiche le nombre de batons total
     * @param tableau contenant les différents batons aux différentes lignes
     */
    void affichageTas(int[]tas){
        for(int i=0 ; i < tas.length;i++){
            if (tas[i] !=0){
                System.out.print( i + ": ");
                for (int j=0; j < tas[i];j++){
                    System.out.print("|" + " ");
                }
                System.out.println();
            }
        }
    }
    
    /** Methode de test affichageTas
     */
     void testAffichageTas(){
         System.out.println ();
			 System.out.println ("*** testAffichageTas()");
			 int[] t1 = {8};
             affichageTas (t1);
             int[] t2 = {1,2,3,4,5,6};
			 affichageTas (t2);
             int[] t3 = {0,0,1};
             affichageTas (t3);
    }
    
   /** Permet de diviser le nombre de baton sur une ligne
    * @param tableau contenant le nombre de batons
    * @param ligne ou l'on veut diviser
    * @param nombre de baton que l'on garde sur cette ligne
    */ 
     void diviseTableau( int[]tas,int ligne, int coupe){
         int temp = tas[ligne];
         tas[ligne] = coupe;
         for (int i=0; i< tas.length; i++){
             if (tas[i] == 0){
                 tas[i]= temp - coupe;
                 i = tas.length;
            }
        }   
    }
    
    /**teste de la méthode diviseTableau
    */
    void testDiviseTableau () {
        System.out.println ();
        System.out.println ("*** testDiviseTableau()");
        int[]t = {7,2,0,0,0,0,0,0};
        int[]t2 = {1,2,6,0,0,0,0,0};
        testCasDiviseTableau(t,0,1,t2);
        int[]t3 = {1,2,1,1,4,0,0,0};
        int[]t4 = {1,2,1,1,3,1,0,0};
        testCasDiviseTableau(t3,4,3,t4);
        int[]t5 = {8,0,0,0,0,0,0,0};
        int[]t6 = {1,7,0,0,0,0,0,0};
        testCasDiviseTableau(t5,0,1,t6);
    }
	
    /*** teste un appel de diviseTableau
    * @param tableau contenant les tas de batons
    * @param ligne ou l'on divise
    * @param coupe ce que l'on souhaite garder sur la ligne
    * @param result le tableau  attendu
    */
    void testCasDiviseTableau (int[] tas, int ligne,int coupe,int[] result) {
        // Act
        diviseTableau(tas,ligne,coupe);
        // Arrange
        System.out.print ("diviseTableau(" +Arrays.toString( tas )+", "+ ligne +", "+ coupe +" )");
        // Assert			 
        if (Arrays.equals(tas,result)){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
    
    /** Vérifie si au moins un coup est possible
     *@param tableau contenant le nombres de batons aux différentes lignes
     * @return vrai ssi aucun coup n'est possible
     */
     boolean estFini ( int[] tas){
         boolean fini = true;
         for ( int i=0; i < tas.length; i++){
             if ( tas[i] != 0 &&  tas[i] != 1 && tas[i] != 2){
                 fini = false;
            }
        }
        return fini;
    }
    
    /**teste de la méthode estFini
		 */
    void testEstFini() {
        System.out.println ();
        System.out.println ("*** testEstFini()");
        int[]t = {1,2,1,2,1,2,0,0};
        testCasEstFini(t,true);
        int[]t2 = {1,1,1,1,1,1,1,2};
        testCasEstFini(t2,true);
        int[]t3 = {1,1,1,5,0,0,0,0};
        testCasEstFini(t3,false);
        int[]t4 = {1,1,1,1,1,4,0,0};
        testCasEstFini(t4,false);
        int[]t5 = {8,0,0,0,0,0,0,0};
        testCasEstFini(t5,false);
    }
	
    /*** teste un appel de estFini
    * @param tableau contenant les batons
    * @param result le resultat attendu
    */
    void testCasEstFini(int[] tas,boolean result) {
        // Arrange
        System.out.print ("estFini(" +Arrays.toString( tas )+" )");
        // Act
        boolean resExec = estFini(tas);
        // Assert			 
        if (resExec==result){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
    
    /** Choix du type de jeu : Joueur vs Joueur ou Ordinateur vs Joueur
     */
     int choixTypeJeu (){
         int type;
         System.out.println("Choisissez le type de jeu :");
         System.out.println(" 1 : Joueur contre joueur");
         System.out.println(" 2 : OrdinateurAléatoire contre joueur");
         System.out.println(" 3 : OrdinateurOptimisé contre joueur");
         type = SimpleInput.getInt(" Entrez le type de jeu que "+
            "vous voulez jouer");
        while ( type != 1 && type != 2 && type != 3){
             type = SimpleInput.getInt(" Entrez le type de jeu que "+
            "vous voulez jouer");
        }
        return type;
    }
    
    /** Partie joueur contre joueur
     */
    void partieJoueur (){
        int ligne;
        int coupe;
        int i = 0;
        int[] tas = tasDepart ();
        String joueur1 = SimpleInput.getString(" Entrez le nom du joueur 1 :");
        String joueur2 = SimpleInput.getString(" Entrez le nom du joueur 2 :");
        String joueurActuel = joueur1;
        affichageTas(tas);
        while ( estFini(tas)==false){
            System.out.println(" Tour de : " + joueurActuel);
            if ( i == 0){
                ligne = 0;
            } else {
                ligne = SimpleInput.getInt("Entrez la ligne où vous voulez jouer :");
                // Condition sortie: ligne >=0 && ligne < tas.length && tas[ligne] >= 3
                while (ligne < 0 || ligne >= tas.length || tas[ligne] < 3 ){
                    ligne = SimpleInput.getInt("Entrez la ligne où vous voulez jouer :");
                }
            }
            coupe = SimpleInput.getInt("Entrez le nombre de batons que vous"+ 
                " voulez laissez sur cette ligne :");
            // Condition de sortie : coupe < tas[ligne] && coupe > 0 
            while ( coupe >= tas[ligne] || coupe <= 0 || 
                (tas[ligne] % 2 == 0 && coupe == tas[ligne]/2)){
                coupe = SimpleInput.getInt("Entrez le nombre de batons que vous"+ 
                    " voulez laissez sur cette ligne :");
            }
            diviseTableau(tas,ligne,coupe);
            joueurActuel = changementJoueur (joueurActuel, joueur1, joueur2);
            affichageTas(tas);
            i = i + 1;
        }
        if ( joueurActuel == joueur1){
            // A la fin du tour du joueur gagnant, il y a changement de joueurActuel
            // Donc le joueur gagnant n'est pas le joueurActuel
            System.out.println(" Bravo à " + joueur2 +" qui remporte la partie !");
        } else{
            System.out.println(" Bravo à " + joueur1 +" qui remporte la partie !");
        }
    }

    /** Partie Ordinateur vs Joueur
     */
    void partieOrdinateurAleatoire(){
        int ligne;
        int coupe;
        int[] tas = tasDepart ();
        String joueur = SimpleInput.getString(" Entrez le nom du joueur  :");
        String ordinateur = "Ordinateur";
        String joueurActuel = ordinateur;
        affichageTas(tas);
        while ( estFini(tas)==false){
            System.out.println(" Tour de : " + joueurActuel);
            if ( joueurActuel == joueur){
                ligne = SimpleInput.getInt("Entrez la ligne où vous voulez jouer :");
                // Condition sortie: ligne >=0 && ligne < tas.length && tas[ligne] >= 3
                while (ligne < 0 || ligne >= tas.length || tas[ligne] < 3 ){
                    ligne = SimpleInput.getInt("Entrez la ligne où vous voulez jouer :");
                }
                    
                coupe = SimpleInput.getInt("Entrez le nombre de batons que vous"+ 
                    " voulez laissez sur cette ligne :");
                // Condition de sortie : coupe < tas[ligne] && coupe > 0 
                while ( coupe >= tas[ligne] || coupe <= 0 || 
                    (tas[ligne] % 2 == 0 && coupe == tas[ligne]/2)){
                    coupe = SimpleInput.getInt("Entrez le nombre de batons que vous"+ 
                        " voulez laissez sur cette ligne :");
                }
            } else {
                int [] coupOpti = tourOrdinateur(tas);
                ligne = coupOpti[0];
                coupe = coupOpti [1];
            }
            diviseTableau(tas,ligne,coupe);
            joueurActuel = changementJoueur (joueurActuel, joueur, ordinateur);
            affichageTas(tas);
        }
        if ( joueurActuel == joueur){
            // A la fin du tour du joueur gagnant, il y a changement de joueurActuel
            // Donc le joueur gagnant n'est pas le joueurActuel
            System.out.println(" Bravo à " + ordinateur +" qui remporte la partie !");
        } else{
            System.out.println(" Bravo à " + joueur +" qui remporte la partie !");
        }
    }
    
    
    /** Méthode qui fait jouer l'ordinateur
     * coup aléatoire sauf si valeur du tableau trop grande
     * alors valeur divisé par 4
     * @param tas tableau contenant les batons
     * @return coupOpti tableau contenant en indice 1 la ligne où jouer
     * et en indice 2 la coupe à faire
     */
     int[] tourOrdinateur (int[] tas ){
         int [] coupOpti = {0,0};
         boolean tropGrand = false;
         int max = 1;
         int indiceMax = 0;
         int ligne = 0;
         int coupe = 0;
         for ( int i=0 ; i < tas.length ; i++){
             if ( tas[i] >= 12 ){
                 tropGrand = true;
             }
             if ( tas[i] > max){
                 max = tas[i];
                 indiceMax = i;
            }
        }
        if ( tropGrand){
            coupOpti[0] = indiceMax;
            coupOpti[1] =  (int) max/4;
            // j'ai décidé choisir le quart de la valeur si la valeur est trop grande
        }else{
            ligne = (int) (Math.random() * tas.length);
            while (ligne < 0 || ligne >= tas.length || tas[ligne] < 3 ){
                ligne = (int) (Math.random() * tas.length);
            }
            coupe = (int) (Math.random() * tas[ligne])+1;
            while (coupe >= tas[ligne] || coupe <= 0 || 
                (tas[ligne] % 2 == 0 && coupe == tas[ligne]/2)){
                coupe = (int) (Math.random() * tas[ligne])+1;
            }
            coupOpti[0] = ligne;
            coupOpti[1] = coupe;
        }
        return coupOpti;
    }
    
    /** test la Méthode tourOrdinateur
    */
   void testTourOrdinateur (){
        System.out.println ();
        System.out.println ("*** testTourOrdinateur()");
        int[]t = {1,4,4,1,5,0,0,0,0,0,0,0,0,0};
        System.out.println(Arrays.toString(tourOrdinateur(t)));
        System.out.println(Arrays.toString(tourOrdinateur(t)));
        System.out.println(Arrays.toString(tourOrdinateur(t)));
        System.out.println(Arrays.toString(tourOrdinateur(t)));
        System.out.println(Arrays.toString(tourOrdinateur(t)));
    }
   
    
    /** Partie OrdinateurOptimisé vs Joueur 
     */
    void partieOrdinateurOptimise(){
        int ligne;
        int coupe;
        int[] tas = tasDepart ();
        String joueur = SimpleInput.getString(" Entrez le nom du joueur  :");
        String ordinateur = "Ordinateur Optimisé";
        String joueurActuel = ordinateur;
        affichageTas(tas);
        while ( estFini(tas)==false){
            System.out.println(" Tour de : " + joueurActuel);
            if ( joueurActuel == joueur){
                ligne = SimpleInput.getInt("Entrez la ligne où vous voulez jouer :");
                // Condition sortie: ligne >=0 && ligne < tas.length && tas[ligne] >= 3
                while (ligne < 0 || ligne >= tas.length || tas[ligne] < 3 ){
                    ligne = SimpleInput.getInt("Entrez la ligne où vous voulez jouer :");
                }
                    
                coupe = SimpleInput.getInt("Entrez le nombre de batons que vous"+ 
                    " voulez laissez sur cette ligne :");
                // Condition de sortie : coupe < tas[ligne] && coupe > 0 
                while ( coupe >= tas[ligne] || coupe <= 0 || 
                    (tas[ligne] % 2 == 0 && coupe == tas[ligne]/2)){
                    coupe = SimpleInput.getInt("Entrez le nombre de batons que vous"+ 
                        " voulez laissez sur cette ligne :");
                }
            } else {
                int [] strategie = tourOrdinateurOpti(tas);
                ligne = strategie[0];
                coupe = strategie[1];
            }
            diviseTableau(tas,ligne,coupe);
            joueurActuel = changementJoueur (joueurActuel, joueur, ordinateur);
            affichageTas(tas);
        }
        if ( joueurActuel == joueur){
            // A la fin du tour du joueur gagnant, il y a changement de joueurActuel
            // Donc le joueur gagnant n'est pas le joueurActuel
            System.out.println(" Bravo à " + ordinateur +" qui remporte la partie !");
        } else{
            System.out.println(" Bravo à " + joueur +" qui remporte la partie !");
        }
    }
    
    /** Méthode qui fait jouer l'ordinateurOptimisé
     * coup Optimisé si la valeur est inférieure à 13 et gagnante 
     * sinon  le coup est aléatoire
     * @param tas tableau contenant les batons
     * @return strategie tableau contenant en indice 1 la ligne où jouer
     * et en indice 2 le nombre de batons à laisser sur la ligne
     */
    int[] tourOrdinateurOpti (int[] tas ){
        int [] strategie = {0,0};
        boolean tropGrand = false;
        int ligne = 0;
        int coupe = 0;
        int max = 1;
        int indiceMax = 0;
        int nbTrois = 0;//Variable qui compte le nombre de 3 dans le tableau
        for ( int i=0 ; i < tas.length ; i++){
            if ( tas[i] > 12 ){
                tropGrand = true;
            }
            if ( tas[i] > max){
                 max = tas[i];
                 indiceMax = i;
            }else if ( tas[i] == 3) {
                nbTrois = nbTrois + 1;
            }
       }
       if (tropGrand){
            strategie[0] = indiceMax;
            strategie[1] =  (int) max/4;
            // j'ai décidé choisir le quart de la valeur si la valeur est trop grande
            
       }else if (nbTrois % 2 == 0 || nbTrois == 0 ) {
           // un nombre impair de 3 inverse les coups gagnants et perdants
            if ( max == 12 || max == 9 || max == 6 ){
               strategie[0]=indiceMax;
               strategie[1]=2;
            }else if ( max == 11 || max == 5){
               strategie[0]=indiceMax;
               strategie[1]=4;
            }else if ( max == 8){
               strategie[0]=indiceMax;
               strategie[1]=1;
            }else {
                ligne = (int) (Math.random() * tas.length);
                while (ligne < 0 || ligne >= tas.length || tas[ligne] < 3 ){
                    ligne = (int) (Math.random() * tas.length);
                }
                coupe = (int) (Math.random() * tas[ligne])+1;
                while (coupe >= tas[ligne] || coupe <= 0 || 
                    (tas[ligne] % 2 == 0 && coupe == tas[ligne]/2)){
                    coupe = (int) (Math.random() * tas[ligne])+1;
                }
                strategie[0] = ligne;
                strategie[1] = coupe;
            }

        }else {
            // nombre impair de 3  donc on joue des coups "perdants" normalement
            if ( max == 12 || max == 11 || max == 9){
               strategie[0]=indiceMax;
               strategie[1]=3;
            }else if ( max == 8 || max == 5){
               strategie[0]=indiceMax;
               strategie[1]=2;
            }
            else if (  max == 6 ){
               strategie[0]=indiceMax;
               strategie[1]=1;
            }
            else{
                ligne = (int) (Math.random() * tas.length);
                while (ligne < 0 || ligne >= tas.length || tas[ligne] < 3 ){
                    ligne = (int) (Math.random() * tas.length);
                }
                coupe = (int) (Math.random() * tas[ligne])+1;
                while (coupe >= tas[ligne] || coupe <= 0 || 
                    (tas[ligne] % 2 == 0 && coupe == tas[ligne]/2)){
                    coupe = (int) (Math.random() * tas[ligne])+1;
                }
                strategie[0] = ligne;
                strategie[1] = coupe;
                
            }
        }
       return strategie;
   }
   
   /** test la Méthode tourOrdinateurOpti
    */
   void testTourOrdinateurOpti (){
        System.out.println ();
        System.out.println ("*** testTourOrdinateurOpti()");
        int[]t = {8,0,0,0,0,0};
        int[]t2 = {0,1};
        testCasTourOrdinateurOpti(t,t2);
        int[]t3 = {8,3,0,0,0,0};
        int[]t4 = {0,2};
        testCasTourOrdinateurOpti(t3,t4);
        int[]t5 = {15,0,0,0,0,0,0,0,0,0,0,0,0,0};
        int[]t6 = {0,3};
        testCasTourOrdinateurOpti(t5,t6);
        int[]t7 = {6,3,3,1,2,0,0,0,0,0,0,0,0,0};
        int[]t8 = {0,2};
        testCasTourOrdinateurOpti(t7,t8);

    }
	
    /*** teste un appel de tourOrdinateurOpti
    * @param tableau contenant les batons
    * @param tableau contenant le coup à faire
    */
    void testCasTourOrdinateurOpti(int[] tas,int[] result) {
        int[] resExec = {0,0};
        // Arrange
        System.out.print ("tourOrdinateurOpti(" +Arrays.toString( tas )+" )");
        // Act
        resExec = tourOrdinateurOpti(tas);
        System.out.println(Arrays.toString( resExec ));
        // Assert			 
        if (Arrays.equals(resExec,result)){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }                
}

        
